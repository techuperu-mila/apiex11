package com.techu.api.controller;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import com.techu.api.AuthHandler;
import com.techu.api.JWTBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apitechu/jwt")
public class HelloWorldController {
    @Autowired
    JWTBuilder jwtBuilder;

    @getMapping("/tokenget")
    public String tokenget(@RequestParam(value="nombre", defaultValue="Tech U!") String name) {
        return jwtBuilder.generateToken(name, "admin");
    }

    @getMapping(path="/hello", headers = {"Autorization"})
    @before(@BeforeElement(AuthHandler.class))
    public String helloWorld(){
        String s = "hello from JWT demo...";
        return s;
    }

}
